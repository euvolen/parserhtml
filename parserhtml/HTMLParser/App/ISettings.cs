﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTMLParser.App
{
    interface ISettings
    {
        string CoreUrl { get; set; }

        string Prefix { get; set; }

        int Start { get; set; }
        int Finish { get; set; }
    }
}
