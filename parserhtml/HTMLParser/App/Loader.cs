﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace HTMLParser.App.Site
{
    class Loader
    {
        readonly HttpClient client;
        readonly string url;


        public Loader(ISettings settings)
        {
            client = new HttpClient();
            url = $"{settings.CoreUrl}/{settings.Prefix}/";
        }

        public async Task<string> GetLinkById(int id)
        {
             var currentUrl = url.Replace("{CurrentId}", id.ToString());
            var response = await client.GetAsync(currentUrl);
            string source = null;

            if(response != null && response.StatusCode == HttpStatusCode.OK)
            {
                source = await response.Content.ReadAsStringAsync();
            }

            return source;
        }
    }
}
