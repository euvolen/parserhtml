﻿using System;
using AngleSharp.Parser.Html;
using HTMLParser.App.Site;

namespace HTMLParser.App
{
    class Parser <T> where T : class
    {
        IParse<T> parse;
        ISettings settings;
        Loader loader;
        bool isActive;

        public event Action<object, T> OnNewData;
        public event Action<object> OnCompleted;

        public Parser(IParse<T> parse)
        {
            this.parse = parse;
           
        }

        public Parser(IParse<T> parse, ISettings settings):this(parse)
        {
               this.settings = settings;
        }

        public bool IsActive { get; }
        public IParse<T> Parse { get => parse; set => parse = value; }

        public ISettings Settings
        {
            get => settings;
            set
            {
                settings = value;
                loader = new Loader(value);
            }
        }

        public void StartParse()
        {
            isActive = true;
            Worker();
        }

        public void StopParse()
        {
            isActive = false;
        }

        private async void Worker()
        {
            for (int i = Settings.Start; i <= Settings.Finish; i++)
            {
                if (!isActive)
                {
                    OnCompleted?.Invoke(this);
                    return;
                }

                var source = await loader.GetLinkById(i);
                var dp = new HtmlParser();

                var doc = await dp.ParseAsync(source);

                var result = parse.Parse(doc);
                Console.WriteLine(result);
                OnNewData?.Invoke(this,result);
            }
            OnCompleted?.Invoke(this);
            isActive = false;
        }
    }
}
