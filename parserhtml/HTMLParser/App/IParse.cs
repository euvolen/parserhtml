﻿using AngleSharp.Dom.Html;

namespace HTMLParser.App
{
    interface IParse<T> where T : class
    {
        T Parse(IHtmlDocument document);


    }
}
