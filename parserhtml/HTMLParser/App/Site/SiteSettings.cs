﻿
namespace HTMLParser.App.Site
{
    class SiteSettings: ISettings
    {
        public SiteSettings(int start, int finish)
        {
            Start = start;
            Finish = finish;

        }
        public string CoreUrl { get; set; } = "https://www.cadmatic.com/en/company/news/";
        public string Prefix { get; set; } = "?page={CurrentId}";
        public int Start { get; set; }
        public int Finish { get; set; }
    }
}
