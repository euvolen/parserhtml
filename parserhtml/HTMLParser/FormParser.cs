﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HTMLParser.App;
using HTMLParser.App.Site;

namespace HTMLParser
{
    public partial class FormParser : Form
    {
        Parser <string[]>parser; 


        public FormParser()
        {
            InitializeComponent();

            parser = new Parser<string[]>(new SiteParser());

            parser.OnCompleted += Parser_OnCompleted;
            parser.OnNewData += Parser_OnNewData;
        }

        private void Parser_OnNewData(object arg1, string[] arg2)
        {
            ListTitles.Items.AddRange(arg2);
        }

        private void Parser_OnCompleted(object obj)
        {
            MessageBox.Show("Completed!");
        }

    
        private void ButtonStart_Click(object sender, EventArgs e)
        {
            parser.Settings = new SiteSettings(
            (int)numericStart.Value, (int)numericEnd.Value);
            parser.StartParse();
        }

        private void ButtonEnd_Click(object sender, EventArgs e)
        {
            parser.StopParse();
        }

    }
}
